var exec=require("child_process").exec;
var sprintf=require("sprintf-js").sprintf;
var configDir=require("./configDir");
var config=require(configDir);

module.exports = function (data) {
    var repo=data.repository.name,
        repoCfg=config.repositories[repo],
        name=repoCfg.name,
        basepath=repoCfg.basepath,
        command=repoCfg.command,
        branch=repoCfg.branch,
        msg, cmd, proc, deploy;

    data.commits.forEach(function(commit) {

        if(commit.branch == branch){
           deploy = true;
        } else {
           deploy = false;
           msg=sprintf(
            "Branch '%s' is not defined, skipping.",
            commit.branch);
            console.log(msg);
        }
    });

    if(!deploy) {
        return undefined;
    }

    if(!repoCfg) {
        msg=sprintf(
            "Configuration for '%s' doesn't exist",
            repo);
        console.log(msg);
        return undefined;
    }

    cmd=sprintf(
        "cd %(basepath)s && %(command)s",
        repoCfg);

    console.log("Executing " + cmd +" process "+name);
    proc=execCommand(cmd);

    proc.on('exit', function (code, signal) {
        console.log(sprintf("command %s exited with code %d", command, code));
        if (code != 0) {
            command_ans="cd /home/deploy/ansible/current && ansible-playbook -i staging tools/notify.yml --tags slack -e 'slack_msg=\"*FAILED!*, please fix it or ask for help.\" stage_url=\""+sprintf("%s", cmd)+"\" error_msg=\""+sprintf("%s", code)+"\"'";
            console.log(command_ans);
            execCommand(command_ans);
       }
    });

    return proc;
}

function execCommand(cmd) {
   return exec(cmd, function(error, stdout, stderr) {
    console.log('stdout: ', stdout);
    console.log('stderr: ', stderr);
        if (error !== null) {
            console.log('exec error: ', error);
        }
    });

}
